using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CalcMain : MonoBehaviour
{
    [SerializeField]
    private TextMeshPro display;

    private string displayNum = "";

    private float prevTotal = 0f;
    private int opCode = -1;

    private void Awake()
    {
        if(!display)
        {
            Debug.LogError("Display not attached!");
        }
    }

    public void ClearDisplay()
    {
        prevTotal = 0f;
        opCode = -1;
        displayNum = "";
    }
    
    private void UpdateDisplay()
    {
        display.text = ""+displayNum;
    }


    public void PlusButton()
    {
        opCode = 0;
        prevTotal = float.Parse(displayNum);
        displayNum = "";
        UpdateDisplay();
    }

    public void MinusButton()
    {
        opCode = 1;
        prevTotal = float.Parse(displayNum);
        displayNum = "";
        UpdateDisplay();
    }

    public void MultButton()
    {
        opCode = 2;
        prevTotal = float.Parse(displayNum);
        displayNum = "";
        UpdateDisplay();
    }

    public void DivButton()
    {
        opCode = 3;
        prevTotal = float.Parse(displayNum);
        displayNum = "";
        UpdateDisplay();
    }

    public void Compute()
    {
        var ans = 0f;
        switch(opCode)
        {
            case 0:
                ans = prevTotal + float.Parse(displayNum);           
                break;
            case 1:
                ans = prevTotal - float.Parse(displayNum);
                break;
            case 2:
                ans = prevTotal * float.Parse(displayNum);
                break;
            case 3:
                ans = prevTotal / float.Parse(displayNum);
                break;
            default: return;
        }
        opCode = -1;
        displayNum = ans.ToString();
        UpdateDisplay();
    }

    public void Num1()
    {
        displayNum = displayNum + "1";
        UpdateDisplay();
    }

    public void Num2()
    {
        displayNum = displayNum + "2";
        UpdateDisplay();
    }

    public void Num3()
    {
        displayNum = displayNum + "3";
        UpdateDisplay();
    }

    public void Num4()
    {
        displayNum = displayNum + "4";
        UpdateDisplay();
    }

    public void Num5()
    {
        displayNum = displayNum + "5";
        UpdateDisplay();
    }

    public void Num6()
    {
        displayNum = displayNum + "6";
        UpdateDisplay();
    }

    public void Num7()
    {
        displayNum = displayNum + "7";
        UpdateDisplay();
    }

    public void Num8()
    {
        displayNum = displayNum + "8";
        UpdateDisplay();
    }

    public void Num9()
    {
        displayNum = displayNum + "9";
        UpdateDisplay();
    }

    public void Num0()
    {
        displayNum = displayNum + "0";
        UpdateDisplay();
    }

    public void NumDot()
    {
        if(displayNum.Contains("."))
        {
            return;
        }
        displayNum = displayNum + ".";
        UpdateDisplay();
    }
}
